## ViewPagerIndicator

## Introduction

Paging indicator widgets that are compatible with the `PageSlider` from
openharmony to improve discoverability of content.

## Usage Instructions

Can be used to create PageSlider with below set of types.

- Circles
    - Default
    - Initial Page
    - Styled Via Methods
    - Styled Via Layouts
    - With Listener
    
- Icons
    - Default

- Lines
    - Default
    - Styled Via Methods
    - Styled Via Layouts
    
- Tabs
    - Default
    - With Icons
    
- Titles
    - Center Click Listener
    - Default
    - Default Bottom
    - Initial Page
    - Styled Via Methods
    - Styled Via Layouts
    - Triangle Style
    - With Listener
    
- Under Lines
    - Default
    - No Fade
    - Styled Via Methods
    - Styled Via Layouts
    - With Listener
    

## Installation instruction

```
Solution 1: local har package integration
Add the .har package to the lib folder.
Add the following code to the gradle of the entry:
implementation fileTree(dir: 'libs', include: ['.jar', '.har'])

Solution 2: Add following dependencies in your build.gradle:

allprojects {
    repositories {
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:viewpagerindicator:1.0.0'

```


## Licence
-----------------------------------------------------------------------
Copyright 2012 Jake Wharton
Copyright 2011 Patrik Åkerfeldt
Copyright 2011 Francisco Figueiredo Jr.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.