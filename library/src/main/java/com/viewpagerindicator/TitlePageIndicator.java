/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.viewpagerindicator;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.viewpagerindicator.util.ResUtil;

import java.util.ArrayList;


/**
 * The type Title page indicator.
 */
public class TitlePageIndicator extends Component implements PageIndicator, Component.DrawTask,
        Component.TouchEventListener {
    private static final float SELECTION_FADE_PERCENTAGE = 0.25f;
    private static final float BOLD_FADE_PERCENTAGE = 0.05f;
    private static final String EMPTY_TITLE = "";

    // custom attributes
    private static final String INDICATOR_BG_COLOR = "indicator_bgColor";
    private static final String INDICATOR_FOOTER_COLOR = "indicator_footerColor";
    private static final String INDICATOR_TEXT_COLOR = "indicator_textColor";
    private static final String INDICATOR_SELECTED_TEXT_COLOR = "indicator_selectedTextColor";
    private static final String INDICATOR_SELECTED_TEXT_BOLD = "indicator_selectedTextBold";
    private static final int INVALID_POINTER = -1;
    private final Paint mPaintText = new Paint();
    private final Rect mBounds = new Rect();
    private final Paint mPaintFooterLine = new Paint();
    private final Paint mPaintFooterIndicator = new Paint();
    private PageSlider mViewPager;
    private PageSlider.PageChangedListener mListener;
    private int mCurrentPage = -1;
    private float mPageOffset;
    private int mScrollState;
    private boolean mBoldText;
    private int mColorText;
    private int mColorSelected;
    private Path mPath = new Path();
    private TitlePageIndicator.IndicatorStyle mFooterIndicatorStyle;
    private TitlePageIndicator.LinePosition mLinePosition;
    private float mFooterIndicatorHeight;
    private float mFooterIndicatorUnderlinePadding;
    private float mFooterPadding;
    private float mTitlePadding;
    private float mTopPadding;
    /**
     * Left and right side padding for not active view titles.
     */
    private float mClipPadding;
    private float mFooterLineHeight;
    private float mPaintTextSize;
    private int mPaintFooterIndicatorColor;
    private int mTouchSlop;
    private int colorBg;
    private int colorFooter;
    private int colorText;
    private int colorSelectedText;
    private boolean isBoldText;
    private float mLastMotionX = -1;
    private int mActivePointerId = INVALID_POINTER;
    private boolean mIsDragging;
    private TitlePageIndicator.OnCenterItemClickListener mCenterItemClickListener;
    private float prevPosition = 0;

    /**
     * Instantiates a new Title page indicator.
     *
     * @param context the context
     */
    public TitlePageIndicator(Context context) {
        this(context, null);
    }

    /**
     * Instantiates a new Title page indicator.
     *
     * @param context the context
     * @param attrSet the attr set
     */
    public TitlePageIndicator(Context context, AttrSet attrSet) {
        super(context, attrSet);

        // DrawTask and TouchListener
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);

        // Load defaults from resources
        loadDefaults(context, attrSet);

        final float textSize = mPaintTextSize;
        final int footerColor = mPaintFooterIndicatorColor;
        mPaintText.setTextSize((int) textSize);
        mPaintText.setAntiAlias(true);
        mPaintFooterLine.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        mPaintFooterLine.setStrokeWidth(mFooterLineHeight);
        mPaintFooterLine.setColor(new Color(footerColor));
        mPaintFooterIndicator.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        mPaintFooterIndicator.setColor(new Color(footerColor));

        Element background = ResUtil.buildDrawableByColor(Color.DKGRAY.getValue());
        if (background != null) {
            setBackground(background);
        }
    }

    private void loadDefaults(Context context, AttrSet attrSet) {
        final int defaultFooterColor = ResUtil.getColor(context,
                ResourceTable.Color_default_title_indicator_footer_color);
        final float defaultFooterLineHeight = ResUtil.getDimen(context,
                ResourceTable.Float_default_title_indicator_footer_line_height);
        final int defaultFooterIndicatorStyle = ResUtil.getInt(context,
                ResourceTable.Integer_default_title_indicator_footer_indicator_style);
        final float defaultFooterIndicatorHeight = ResUtil.getDimen(context,
                ResourceTable.Float_default_title_indicator_footer_indicator_height);
        final float defaultFooterIndicatorUnderlinePadding = ResUtil.getDimen(context,
                ResourceTable.Float_default_title_indicator_footer_indicator_underline_padding);
        final float defaultFooterPadding = ResUtil.getDimen(context,
                ResourceTable.Float_default_title_indicator_footer_padding);
        final int defaultLinePosition = ResUtil.getInt(context,
                ResourceTable.Integer_default_title_indicator_line_position);
        final int defaultSelectedColor = ResUtil.getColor(context,
                ResourceTable.Color_default_title_indicator_selected_color);
        final boolean defaultSelectedBold = ResUtil.getBoolean(context,
                ResourceTable.Boolean_default_title_indicator_selected_bold);
        final int defaultTextColor = ResUtil.getColor(context,
                ResourceTable.Color_default_title_indicator_text_color);
        final float defaultTextSize = ResUtil.getDimen(context,
                ResourceTable.Float_default_title_indicator_text_size);
        final float defaultTitlePadding = ResUtil.getDimen(context,
                ResourceTable.Float_default_title_indicator_title_padding);
        final float defaultClipPadding = ResUtil.getDimen(context,
                ResourceTable.Float_default_title_indicator_clip_padding);
        final float defaultTopPadding = ResUtil.getDimen(context,
                ResourceTable.Float_default_title_indicator_top_padding);

        loadAttributeValues(attrSet);

        // Retrieve the colors to be used for this view and apply them.
        mFooterLineHeight = defaultFooterLineHeight;
        mFooterIndicatorStyle = TitlePageIndicator.IndicatorStyle.fromValue(defaultFooterIndicatorStyle);
        mFooterIndicatorHeight = defaultFooterIndicatorHeight;
        mFooterIndicatorUnderlinePadding = defaultFooterIndicatorUnderlinePadding;
        mFooterPadding = defaultFooterPadding;
        mLinePosition = TitlePageIndicator.LinePosition.fromValue(defaultLinePosition);
        mTopPadding = defaultTopPadding;
        mTitlePadding = defaultTitlePadding;
        mClipPadding = defaultClipPadding;
        mColorSelected = defaultSelectedColor;
        mColorText = defaultTextColor;
        mBoldText = defaultSelectedBold;
        mPaintTextSize = defaultTextSize;
        mPaintFooterIndicatorColor = defaultFooterColor;
    }

    private void loadAttributeValues(AttrSet attrSet) {
        if (attrSet.getAttr(INDICATOR_BG_COLOR).isPresent()) {
            colorBg = attrSet.getAttr(INDICATOR_BG_COLOR).get().getColorValue().getValue();
        }

        if (attrSet.getAttr(INDICATOR_FOOTER_COLOR).isPresent()) {
            colorFooter = attrSet.getAttr(INDICATOR_FOOTER_COLOR).get().getColorValue().getValue();
        }

        if (attrSet.getAttr(INDICATOR_TEXT_COLOR).isPresent()) {
            colorText = attrSet.getAttr(INDICATOR_TEXT_COLOR).get().getColorValue().getValue();
        }

        if (attrSet.getAttr(INDICATOR_SELECTED_TEXT_COLOR).isPresent()) {
            colorSelectedText = attrSet.getAttr(INDICATOR_SELECTED_TEXT_COLOR).get().getColorValue().getValue();
        }

        if (attrSet.getAttr(INDICATOR_SELECTED_TEXT_BOLD).isPresent()) {
            isBoldText = attrSet.getAttr(INDICATOR_SELECTED_TEXT_BOLD).get().getBoolValue();
        }
    }

    /**
     * Sets style from layout attributes.
     */
    public void setStyleFromLayoutAttributes() {
        setBackground(ResUtil.buildDrawableByColor(colorBg));
        setFooterColor(colorFooter);
        setTextColor(colorText);
        setSelectedColor(colorSelectedText);
        setSelectedBold(isBoldText);
    }

    /**
     * Gets footer color.
     *
     * @return the footer color
     */
    public int getFooterColor() {
        return mPaintFooterLine.getColor().getValue();
    }

    /**
     * Sets footer color.
     *
     * @param footerColor the footer color
     */
    public void setFooterColor(int footerColor) {
        mPaintFooterLine.setColor(new Color(footerColor));
        mPaintFooterIndicator.setColor(new Color(footerColor));
        invalidate();
    }

    /**
     * Gets footer line height.
     *
     * @return the footer line height
     */
    public float getFooterLineHeight() {
        return mFooterLineHeight;
    }

    /**
     * Sets footer line height.
     *
     * @param footerLineHeight the footer line height
     */
    public void setFooterLineHeight(float footerLineHeight) {
        mFooterLineHeight = footerLineHeight;
        mPaintFooterLine.setStrokeWidth(mFooterLineHeight);
        invalidate();
    }

    /**
     * Gets footer indicator height.
     *
     * @return the footer indicator height
     */
    public float getFooterIndicatorHeight() {
        return mFooterIndicatorHeight;
    }

    /**
     * Sets footer indicator height.
     *
     * @param footerTriangleHeight the footer triangle height
     */
    public void setFooterIndicatorHeight(float footerTriangleHeight) {
        mFooterIndicatorHeight = footerTriangleHeight;
        invalidate();
    }

    /**
     * Gets footer indicator padding.
     *
     * @return the footer indicator padding
     */
    public float getFooterIndicatorPadding() {
        return mFooterPadding;
    }

    /**
     * Sets footer indicator padding.
     *
     * @param footerIndicatorPadding the footer indicator padding
     */
    public void setFooterIndicatorPadding(float footerIndicatorPadding) {
        mFooterPadding = footerIndicatorPadding;
        invalidate();
    }

    /**
     * Gets footer indicator style.
     *
     * @return the footer indicator style
     */
    public TitlePageIndicator.IndicatorStyle getFooterIndicatorStyle() {
        return mFooterIndicatorStyle;
    }

    /**
     * Sets footer indicator style.
     *
     * @param indicatorStyle the indicator style
     */
    public void setFooterIndicatorStyle(TitlePageIndicator.IndicatorStyle indicatorStyle) {
        mFooterIndicatorStyle = indicatorStyle;
        invalidate();
    }

    /**
     * Gets line position.
     *
     * @return the line position
     */
    public TitlePageIndicator.LinePosition getLinePosition() {
        return mLinePosition;
    }

    /**
     * Sets line position.
     *
     * @param linePosition the line position
     */
    public void setLinePosition(TitlePageIndicator.LinePosition linePosition) {
        mLinePosition = linePosition;
        invalidate();
    }

    /**
     * Gets selected color.
     *
     * @return the selected color
     */
    public int getSelectedColor() {
        return mColorSelected;
    }

    /**
     * Sets selected color.
     *
     * @param selectedColor the selected color
     */
    public void setSelectedColor(int selectedColor) {
        mColorSelected = selectedColor;
        invalidate();
    }

    /**
     * Is selected bold boolean.
     *
     * @return the boolean
     */
    public boolean isSelectedBold() {
        return mBoldText;
    }

    /**
     * Sets selected bold.
     *
     * @param selectedBold the selected bold
     */
    public void setSelectedBold(boolean selectedBold) {
        mBoldText = selectedBold;
        invalidate();
    }

    /**
     * Gets text color.
     *
     * @return the text color
     */
    public int getTextColor() {
        return mColorText;
    }

    /**
     * Sets text color.
     *
     * @param textColor the text color
     */
    public void setTextColor(int textColor) {
        mPaintText.setColor(new Color(textColor));
        mColorText = textColor;
        invalidate();
    }

    /**
     * Gets text size.
     *
     * @return the text size
     */
    public float getTextSize() {
        return mPaintText.getTextSize();
    }

    /**
     * Sets text size.
     *
     * @param textSize the text size
     */
    public void setTextSize(float textSize) {
        mPaintText.setTextSize((int) textSize);
        invalidate();
    }

    /**
     * Gets title padding.
     *
     * @return the title padding
     */
    public float getTitlePadding() {
        return this.mTitlePadding;
    }

    /**
     * Sets title padding.
     *
     * @param titlePadding the title padding
     */
    public void setTitlePadding(float titlePadding) {
        mTitlePadding = titlePadding;
        invalidate();
    }

    /**
     * Gets top padding.
     *
     * @return the top padding
     */
    public float getTopPadding() {
        return this.mTopPadding;
    }

    /**
     * Sets top padding.
     *
     * @param topPadding the top padding
     */
    public void setTopPadding(float topPadding) {
        mTopPadding = topPadding;
        invalidate();
    }

    /**
     * Gets clip padding.
     *
     * @return the clip padding
     */
    public float getClipPadding() {
        return this.mClipPadding;
    }

    /**
     * Sets clip padding.
     *
     * @param clipPadding the clip padding
     */
    public void setClipPadding(float clipPadding) {
        mClipPadding = clipPadding;
        invalidate();
    }

    /**
     * Gets typeface.
     *
     * @return the typeface
     */
    public Font getTypeface() {
        return mPaintText.getFont();
    }

    /**
     * Sets typeface.
     *
     * @param typeface the typeface
     */
    public void setTypeface(Font typeface) {
        mPaintText.setFont(typeface);
        invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mViewPager == null) {
            return;
        }
        final int count = mViewPager.getProvider().getCount();
        if (count == 0) {
            return;
        }

        // mCurrentPage is -1 on first start and after orientation changed.
        // If so, retrieve the correct index from viewpager.
        if (mCurrentPage == -1 && mViewPager != null) {
            mCurrentPage = mViewPager.getCurrentPage();
        }

        // Calculate views bounds
        ArrayList<Rect> bounds = calculateAllBounds(mPaintText);
        final int boundsSize = bounds.size();

        // Make sure we're on a page that still exists
        if (mCurrentPage >= boundsSize) {
            setCurrentItem(boundsSize - 1);
            return;
        }

        final int countMinusOne = count - 1;
        final float halfWidth = getWidth() / 2f;
        final int left = getLeft();
        final float leftClip = left + mClipPadding;
        final int width = getWidth();
        int height = getHeight();
        final int right = left + width;
        final float rightClip = right - mClipPadding;

        int page = mCurrentPage;
        float offsetPercent;
        mPageOffset = 0;
        if (mPageOffset <= 0.5) {
            offsetPercent = mPageOffset;
        } else {
            page += 1;
            offsetPercent = 1 - mPageOffset;
        }
        final boolean currentSelected = (offsetPercent <= SELECTION_FADE_PERCENTAGE);
        final boolean currentBold = (offsetPercent <= BOLD_FADE_PERCENTAGE);
        final float selectedPercent = (SELECTION_FADE_PERCENTAGE - offsetPercent) / SELECTION_FADE_PERCENTAGE;

        // Verify if the current view must be clipped to the screen
        Rect curPageBound = bounds.get(mCurrentPage);
        float curPageWidth = curPageBound.right - curPageBound.left;
        if (curPageBound.left < leftClip) {
            // Try to clip to the screen (left side)
            clipViewOnTheLeft(curPageBound, curPageWidth, left);
        }
        if (curPageBound.right > rightClip) {
            // Try to clip to the screen (right side)
            clipViewOnTheRight(curPageBound, curPageWidth, right);
        }

        // Left views starting from the current position
        if (mCurrentPage > 0) {
            for (int i = mCurrentPage - 1; i >= 0; i--) {
                Rect bound = bounds.get(i);
                // Is left side is outside the screen
                if (bound.left < leftClip) {
                    int wid = bound.right - bound.left;
                    // Try to clip to the screen (left side)
                    clipViewOnTheLeft(bound, wid, left);
                    // Except if there's an intersection with the right view
                    Rect rightBound = bounds.get(i + 1);
                    // Intersection
                    if (bound.right + mTitlePadding > rightBound.left) {
                        bound.left = (int) (rightBound.left - wid - mTitlePadding);
                        bound.right = bound.left + wid;
                    }
                }
            }
        }
        // Right views starting from the current position
        if (mCurrentPage < countMinusOne) {
            for (int i = mCurrentPage + 1; i < count; i++) {
                Rect bound = bounds.get(i);
                // If right side is outside the screen
                if (bound.right > rightClip) {
                    int wid = bound.right - bound.left;
                    // Try to clip to the screen (right side)
                    clipViewOnTheRight(bound, wid, right);
                    // Except if there's an intersection with the left view
                    Rect leftBound = bounds.get(i - 1);
                    // Intersection
                    if (bound.left - mTitlePadding < leftBound.right) {
                        bound.left = (int) (leftBound.right + mTitlePadding);
                        bound.right = bound.left + wid;
                    }
                }
            }
        }

        // Now draw views
        int colorTextAlpha = mColorText >>> 24;
        for (int i = 0; i < count; i++) {
            // Get the title
            Rect bound = bounds.get(i);
            // Only if one side is visible
            if ((bound.left > left && bound.left < right) || (bound.right > left && bound.right < right)) {
                final boolean currentPage = (i == page);
                final CharSequence pageTitle = getTitle(i);
                // Only set bold if we are within bounds
                mPaintText.setFakeBoldText(currentPage && currentBold && mBoldText);

                // Draw text as unselected
                mPaintText.setColor(new Color(mColorText));
                if (currentPage && currentSelected) {
                    // Fade out/in unselected text as the selected text fades in/out
                    mPaintText.setAlpha(colorTextAlpha - (int) (colorTextAlpha * selectedPercent));
                }

                // Except if there's an intersection with the right view
                if (i < boundsSize - 1) {
                    Rect rightBound = bounds.get(i + 1);
                    // Intersection
                    if (bound.right + mTitlePadding > rightBound.left) {
                        int wid = bound.right - bound.left;
                        bound.left = (int) (rightBound.left - wid - mTitlePadding);
                        bound.right = bound.left + wid;
                    }
                }
                canvas.drawText(mPaintText, pageTitle.toString(), bound.left, bound.bottom + mTopPadding);

                // If we are within the selected bounds draw the selected text
                if (currentPage && currentSelected) {
                    mPaintText.setColor(new Color(mColorSelected));
                    mPaintText.setAlpha((int) ((mColorSelected >>> 24) * selectedPercent));
                    canvas.drawText(mPaintText, pageTitle.toString(), bound.left, bound.bottom + mTopPadding);
                }
            }
        }

        // Draw Footer
        float footerLineHeight = mFooterLineHeight;
        float footerIndicatorLineHeight = mFooterIndicatorHeight;
        if (mLinePosition == TitlePageIndicator.LinePosition.Top) {
            height = 0;
            footerLineHeight = -footerLineHeight;
            footerIndicatorLineHeight = -footerIndicatorLineHeight;
        }

        mPath.reset();
        mPath.moveTo(0, height - footerLineHeight / 2f);
        mPath.lineTo(width, height - footerLineHeight / 2f);
        mPath.close();
        canvas.drawPath(mPath, mPaintFooterLine);

        float heightMinusLine = height - footerLineHeight;
        switch (mFooterIndicatorStyle) {
            case Triangle:
                mPath.reset();
                mPath.moveTo(halfWidth, heightMinusLine - footerIndicatorLineHeight);
                mPath.lineTo(halfWidth + footerIndicatorLineHeight, heightMinusLine);
                mPath.lineTo(halfWidth - footerIndicatorLineHeight, heightMinusLine);
                mPath.close();
                canvas.drawPath(mPath, mPaintFooterIndicator);
                break;

            case Underline:
                if (!currentSelected || page >= boundsSize) {
                    break;
                }

                Rect underlineBounds = bounds.get(page);
                final float rightPlusPadding = underlineBounds.right + mFooterIndicatorUnderlinePadding;
                final float leftMinusPadding = underlineBounds.left - mFooterIndicatorUnderlinePadding;
                final float heightMinusLineMinusIndicator = heightMinusLine - footerIndicatorLineHeight;

                mPath.reset();
                mPath.moveTo(leftMinusPadding, heightMinusLine);
                mPath.lineTo(rightPlusPadding, heightMinusLine);
                mPath.lineTo(rightPlusPadding, heightMinusLineMinusIndicator);
                mPath.lineTo(leftMinusPadding, heightMinusLineMinusIndicator);
                mPath.close();

                canvas.drawPath(mPath, mPaintFooterIndicator);
                break;
        }
    }

    private void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Measure our width in whatever mode specified
        final int measuredWidth = MeasureSpec.getSize(widthMeasureSpec);

        // Determine our height
        float height;
        final int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        if (heightSpecMode == MeasureSpec.PRECISE) {
            // We were told how big to be
            height = MeasureSpec.getSize(heightMeasureSpec);
        } else {
            // Calculate the text bounds
            mBounds.set(0, 0, 0, 0);
            mBounds.bottom = (int) (mPaintText.getFontMetrics().descent - mPaintText.getFontMetrics().descent);
            height = mBounds.bottom - mBounds.top + mFooterLineHeight + mFooterPadding + mTopPadding;
            if (mFooterIndicatorStyle != TitlePageIndicator.IndicatorStyle.None) {
                height += mFooterIndicatorHeight;
            }
        }
        final int measuredHeight = (int) height;
        setWidth(measuredWidth);
        setHeight(measuredHeight);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if ((mViewPager == null) || (mViewPager.getProvider().getCount() == 0)) {
            return false;
        }

        final int action = touchEvent.getAction();
        final int activePointerIndex = touchEvent.getIndex();
        MmiPoint point = touchEvent.getPointerPosition(activePointerIndex);
        return performTouchResponse(action, point, touchEvent, activePointerIndex);
    }

    private boolean performTouchResponse(int action, MmiPoint point, TouchEvent touchEvent, int activePointerIndex) {
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                onPointDown(point, touchEvent, activePointerIndex);
                break;
            case TouchEvent.POINT_MOVE: {
                onPointMove(point);
                break;
            }
            case TouchEvent.CANCEL:
            case TouchEvent.PRIMARY_POINT_UP:
                if (!mIsDragging) {
                    final int count = mViewPager.getProvider().getCount();
                    final int width = getWidth();
                    final float halfWidth = width / 2f;
                    final float sixthWidth = width / 6f;
                    final float leftThird = halfWidth - sixthWidth;
                    final float rightThird = halfWidth + sixthWidth;
                    final float eventX = point.getX();

                    if (eventX < leftThird) {
                        if (mCurrentPage > 0) {
                            if (action != TouchEvent.CANCEL) {
                                mViewPager.setCurrentPage(mCurrentPage - 1);
                            }
                            return true;
                        }
                    } else if (eventX > rightThird) {
                        if (mCurrentPage < count - 1) {
                            if (action != TouchEvent.CANCEL) {
                                mViewPager.setCurrentPage(mCurrentPage + 1);
                            }
                            return true;
                        }
                    } else {
                        if (mCenterItemClickListener != null && action != TouchEvent.CANCEL) {
                            mCenterItemClickListener.onCenterItemClick(mCurrentPage);
                        }
                    }
                }
                mIsDragging = false;
                mActivePointerId = INVALID_POINTER;
                break;
        }
        return true;
    }

    private void onPointDown(MmiPoint point, TouchEvent touchEvent, int activePointerIndex) {
        mActivePointerId = touchEvent.getPointerId(activePointerIndex);
        mLastMotionX = point.getX();
    }

    private void onPointMove(MmiPoint point) {
        final float posX = point.getX();
        final float deltaX = posX - mLastMotionX;

        if (!mIsDragging) {
            if (Math.abs(deltaX) > mTouchSlop) {
                mIsDragging = true;
            }
        }

        if (mIsDragging) {
            mLastMotionX = posX;
        }
    }

    private ArrayList<Rect> calculateAllBounds(Paint paint) {
        ArrayList<Rect> list = new ArrayList<Rect>();
        // For each views (If no values then add a fake one)
        final int count = mViewPager.getProvider().getCount();
        final int width = getWidth();
        final int halfWidth = width / 2;
        for (int i = 0; i < count; i++) {
            Rect bounds = calcBounds(i, paint);
            int boundWidth = bounds.right - bounds.left;
            int boundHeight = bounds.bottom - bounds.top;
            bounds.left = (int) (halfWidth - (boundWidth / 2f) + ((i - mCurrentPage - mPageOffset) * width));
            bounds.right = bounds.left + boundWidth;
            bounds.top = 0;
            bounds.bottom = boundHeight;
            list.add(bounds);
        }

        return list;
    }

    private Rect calcBounds(int index, Paint paint) {
        // Calculate the text bounds
        CharSequence title = getTitle(index);
        Rect bounds = paint.getTextBounds(title.toString());
        return bounds;
    }

    private CharSequence getTitle(int position) {
        CharSequence title = mViewPager.getProvider().getPageTitle(position);
        if (title == null) {
            title = EMPTY_TITLE;
        }
        return title;
    }

    private void clipViewOnTheLeft(Rect curViewBound, float curViewWidth, int left) {
        curViewBound.left = (int) (left + mClipPadding);
        curViewBound.right = (int) (mClipPadding + curViewWidth);
    }

    private void clipViewOnTheRight(Rect curViewBound, float curViewWidth, int right) {
        curViewBound.right = (int) (right - mClipPadding);
        curViewBound.left = (int) (curViewBound.right - curViewWidth);
    }

    /**
     * Bind the indicator to a ViewPager.
     *
     * @param view The PageSlider
     */
    @Override
    public void setPageSlider(PageSlider view) {
        if (mViewPager == view) {
            return;
        }
        if (mViewPager != null) {
            mViewPager.addPageChangedListener(null);
        }
        if (view.getProvider() == null) {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
        mViewPager = view;
        mViewPager.addPageChangedListener(this);
        invalidate();
    }

    /**
     * Bind the indicator to a ViewPager.
     *
     * @param view            The PageSlider
     * @param initialPosition The Initial Page Position
     */
    public void setPageSlider(PageSlider view, int initialPosition) {
        setPageSlider(view);
        setCurrentItem(initialPosition);
    }

    /**
     * <p>Set the current page of both the ViewPager and indicator.</p>
     *
     * <p>This <strong>must</strong> be used if you need to set the page before
     * the views are drawn on screen (e.g., default start page).</p>
     *
     * @param item The Current Page
     */
    public void setCurrentItem(int item) {
        if (mViewPager == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        mViewPager.setCurrentPage(item);
        mCurrentPage = item;
        invalidate();
    }

    /**
     * Sets on center item click listener.
     *
     * @param listener the listener
     */
    public void setOnCenterItemClickListener(TitlePageIndicator.OnCenterItemClickListener listener) {
        mCenterItemClickListener = listener;
    }

    /**
     * Set a page change listener which will receive forwarded events.
     *
     * @param listener The PageChangedListener
     */
    public void setOnPageChangeListener(PageSlider.PageChangedListener listener) {
        mListener = listener;
    }

    /**
     * Notify the indicator that the fragment list has changed.
     */
    public void notifyDataSetChanged() {
        invalidate();
    }

    @Override
    public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
        mCurrentPage = position;
        mPageOffset = positionOffset;
        if (prevPosition != mCurrentPage) {
            prevPosition = mCurrentPage;
        }
        invalidate();

        if (mListener != null) {
            mListener.onPageSliding(position, positionOffset, positionOffsetPixels);
        }
    }

    @Override
    public void onPageSlideStateChanged(int state) {
        mScrollState = state;

        if (mListener != null) {
            mListener.onPageSlideStateChanged(state);
        }
    }

    @Override
    public void onPageChosen(int position) {
        if (mScrollState == PageSlider.SLIDING_STATE_IDLE) {
            mCurrentPage = position;
            invalidate();
        }

        if (mListener != null) {
            mListener.onPageChosen(position);
        }
    }

    /**
     * The enum Indicator style.
     */
    public enum IndicatorStyle {
        /**
         * None indicator style.
         */
        None(0),
        /**
         * Triangle indicator style.
         */
        Triangle(1),
        /**
         * Underline indicator style.
         */
        Underline(2);

        /**
         * The Value.
         */
        public final int value;

        IndicatorStyle(int value) {
            this.value = value;
        }

        /**
         * From value title page indicator . indicator style.
         *
         * @param value the value
         * @return the title page indicator . indicator style
         */
        public static TitlePageIndicator.IndicatorStyle fromValue(int value) {
            for (TitlePageIndicator.IndicatorStyle style : TitlePageIndicator.IndicatorStyle.values()) {
                if (style.value == value) {
                    return style;
                }
            }
            return null;
        }
    }

    /**
     * The enum Line position.
     */
    public enum LinePosition {
        /**
         * Bottom line position.
         */
        Bottom(0),
        /**
         * Top line position.
         */
        Top(1);

        /**
         * The Value.
         */
        public final int value;

        LinePosition(int value) {
            this.value = value;
        }

        /**
         * From value title page indicator . line position.
         *
         * @param value the value
         * @return the title page indicator . line position
         */
        public static TitlePageIndicator.LinePosition fromValue(int value) {
            for (TitlePageIndicator.LinePosition position : TitlePageIndicator.LinePosition.values()) {
                if (position.value == value) {
                    return position;
                }
            }
            return null;
        }
    }

    /**
     * The interface On center item click listener.
     */
    public interface OnCenterItemClickListener {
        /**
         * On center item click.
         *
         * @param position the position
         */
        void onCenterItemClick(int position);
    }
}
