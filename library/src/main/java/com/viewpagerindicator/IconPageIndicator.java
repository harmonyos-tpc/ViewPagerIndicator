/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.viewpagerindicator;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

/**
 * The type Icon page indicator.
 */
public class IconPageIndicator extends PagerSlidingTab {
    /**
     * Instantiates a new Icon page indicator.
     *
     * @param context the context
     */
    public IconPageIndicator(Context context) {
        super(context);
        init();
    }

    /**
     * Instantiates a new Icon page indicator.
     *
     * @param context the context
     * @param attrSet the attr set
     */
    public IconPageIndicator(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    /**
     * Instantiates a new Icon page indicator.
     *
     * @param context   the context
     * @param attrSet   the attr set
     * @param styleName the style name
     */
    public IconPageIndicator(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        setIsTextShown(false);
        setIndicatorType(INDICATOR_INVISIBLE);
    }

}
