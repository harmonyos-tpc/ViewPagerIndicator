/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.viewpagerindicator;

import ohos.agp.components.AttrSet;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.app.Context;

import com.viewpagerindicator.util.ResUtil;

import java.util.Optional;

/**
 * The type Circle page indicator.
 */
public class CirclePageIndicator extends PageSliderIndicatorExt {
    private static final String TAG = CirclePageIndicator.class.getSimpleName();

    // custom attributes
    private static final String INDICATOR_NORMAL_COLOR = "indicator_normalColor";
    private static final String INDICATOR_SELECTED_COLOR = "indicator_selectedColor";
    private static final String INDICATOR_STROKE_COLOR = "indicator_strokeColor";
    private static final String INDICATOR_NORMAL_STROKE_WIDTH = "indicator_normalStrokeWidth";
    private static final String INDICATOR_SELECTED_STROKE_WIDTH = "indicator_selectedStrokeWidth";
    private static final String INDICATOR_RADIUS = "indicator_radius";

    private Color indicatorNormalColor;
    private Color indicatorSelectedColor;
    private Color indicatorStrokeColor;
    private int indicatorNormalStrokeWidth;
    private int indicatorSelectedStrokeWidth;
    private int indicatorRadius;
    private AttrSet mAttrSet;

    /**
     * Instantiates a new Circle page indicator.
     *
     * @param context the context
     */
    public CirclePageIndicator(Context context) {
        super(context);
        init(context, Optional.empty());
    }

    /**
     * Instantiates a new Circle page indicator.
     *
     * @param context the context
     * @param attrSet the attr set
     */
    public CirclePageIndicator(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, Optional.of(attrSet));
    }

    /**
     * Instantiates a new Circle page indicator.
     *
     * @param context   the context
     * @param attrSet   the attr set
     * @param styleName the style name
     */
    public CirclePageIndicator(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, Optional.of(attrSet));
    }

    private void init(Context context, Optional<AttrSet> optionalAttrSet) {
        if (optionalAttrSet.isPresent()) {
            mAttrSet = optionalAttrSet.get();
            indicatorNormalColor = mAttrSet.getAttr(INDICATOR_NORMAL_COLOR).isPresent() ?
                    mAttrSet.getAttr(INDICATOR_NORMAL_COLOR).get().getColorValue() : new Color(0xFF888888);
            indicatorSelectedColor = mAttrSet.getAttr(INDICATOR_SELECTED_COLOR).isPresent() ?
                    mAttrSet.getAttr(INDICATOR_SELECTED_COLOR).get().getColorValue() : Color.WHITE;
            indicatorStrokeColor = mAttrSet.getAttr(INDICATOR_STROKE_COLOR).isPresent() ?
                    mAttrSet.getAttr(INDICATOR_STROKE_COLOR).get().getColorValue() : new Color(0xFF000000);
            indicatorNormalStrokeWidth = mAttrSet.getAttr(INDICATOR_NORMAL_STROKE_WIDTH).isPresent() ?
                    mAttrSet.getAttr(INDICATOR_NORMAL_STROKE_WIDTH).get().getDimensionValue() : 1;
            indicatorSelectedStrokeWidth = mAttrSet.getAttr(INDICATOR_SELECTED_STROKE_WIDTH).isPresent() ?
                    mAttrSet.getAttr(INDICATOR_SELECTED_STROKE_WIDTH).get().getDimensionValue() : 1;
            indicatorRadius = mAttrSet.getAttr(INDICATOR_RADIUS).isPresent() ?
                    mAttrSet.getAttr(INDICATOR_RADIUS).get().getDimensionValue() : 12;
        } else {
            indicatorNormalColor = new Color(0xFF888888);
            indicatorSelectedColor = Color.WHITE;
            indicatorStrokeColor  = new Color(0xFF000000);
            indicatorNormalStrokeWidth = 1;
            indicatorSelectedStrokeWidth = 1;
            indicatorRadius = 12;
        }
        setItemElement(
                ResUtil.getCustomCircleGradientDrawable(indicatorNormalStrokeWidth, indicatorNormalColor,
                        indicatorStrokeColor, new Rect(0, 0, indicatorRadius * 2, indicatorRadius * 2)),
                ResUtil.getCustomCircleGradientDrawable(indicatorSelectedStrokeWidth, indicatorSelectedColor,
                        indicatorStrokeColor, new Rect(0, 0, indicatorRadius * 2, indicatorRadius * 2)));
    }

    /**
     * Gets indicator normal color.
     *
     * @return the indicator normal color
     */
    public Color getIndicatorNormalColor() {
        return indicatorNormalColor;
    }

    /**
     * Sets indicator normal color.
     *
     * @param indicatorNormalColor the indicator normal color
     */
    public void setIndicatorNormalColor(Color indicatorNormalColor) {
        this.indicatorNormalColor = indicatorNormalColor;
    }

    /**
     * Gets indicator selected color.
     *
     * @return the indicator selected color
     */
    public Color getIndicatorSelectedColor() {
        return indicatorSelectedColor;
    }

    /**
     * Sets indicator selected color.
     *
     * @param indicatorSelectedColor the indicator selected color
     */
    public void setIndicatorSelectedColor(Color indicatorSelectedColor) {
        this.indicatorSelectedColor = indicatorSelectedColor;
    }

    /**
     * Gets indicator stroke color.
     *
     * @return the indicator stroke color
     */
    public Color getIndicatorStrokeColor() {
        return indicatorStrokeColor;
    }

    /**
     * Sets indicator stroke color.
     *
     * @param indicatorStrokeColor the indicator stroke color
     */
    public void setIndicatorStrokeColor(Color indicatorStrokeColor) {
        this.indicatorStrokeColor = indicatorStrokeColor;
    }

    /**
     * Gets indicator normal stroke width.
     *
     * @return the indicator normal stroke width
     */
    public int getIndicatorNormalStrokeWidth() {
        return indicatorNormalStrokeWidth;
    }

    /**
     * Sets indicator normal stroke width.
     *
     * @param indicatorNormalStrokeWidth the indicator normal stroke width
     */
    public void setIndicatorNormalStrokeWidth(int indicatorNormalStrokeWidth) {
        this.indicatorNormalStrokeWidth = indicatorNormalStrokeWidth;
    }

    /**
     * Gets indicator selected stroke width.
     *
     * @return the indicator selected stroke width
     */
    public int getIndicatorSelectedStrokeWidth() {
        return indicatorSelectedStrokeWidth;
    }

    /**
     * Sets indicator selected stroke width.
     *
     * @param indicatorSelectedStrokeWidth the indicator selected stroke width
     */
    public void setIndicatorSelectedStrokeWidth(int indicatorSelectedStrokeWidth) {
        this.indicatorSelectedStrokeWidth = indicatorSelectedStrokeWidth;
    }

    /**
     * Gets indicator radius.
     *
     * @return the indicator radius
     */
    public int getIndicatorRadius() {
        return indicatorRadius;
    }

    /**
     * Sets indicator radius.
     *
     * @param indicatorRadius the indicator radius
     */
    public void setIndicatorRadius(int indicatorRadius) {
        this.indicatorRadius = indicatorRadius;
    }

    /**
     * Sets page slider.
     *
     * @param pageSlider the page slider
     */
    public void setPageSlider(PageSlider pageSlider) {
        super.setPageSlider(pageSlider);
    }

    /**
     * Sets item offset.
     *
     * @param offset the offset
     */
    public void setItemOffset(int offset) {
        super.setItemOffset(offset);
    }

    /**
     * Sets item element.
     *
     * @param normal   the normal
     * @param selected the selected
     */
    public void setItemElement(Element normal, Element selected) {
        super.setItemElement(normal, selected);
    }


}
