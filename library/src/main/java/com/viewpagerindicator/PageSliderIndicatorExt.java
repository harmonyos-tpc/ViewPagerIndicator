/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.viewpagerindicator;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderIndicator;
import ohos.agp.components.element.Element;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;


/**
 * The type Page slider indicator ext.
 */
public class PageSliderIndicatorExt extends ComponentContainer {
    private static final String TAG = PageSliderIndicatorExt.class.getSimpleName();

    private PageSliderIndicator mPageSliderIndicator;

    /**
     * Instantiates a new Page slider indicator ext.
     *
     * @param context the context
     */
    public PageSliderIndicatorExt(Context context) {
        this(context, null);
    }

    /**
     * Instantiates a new Page slider indicator ext.
     *
     * @param context the context
     * @param attrSet the attr set
     */
    public PageSliderIndicatorExt(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * Instantiates a new Page slider indicator ext.
     *
     * @param context   the context
     * @param attrSet   the attr set
     * @param styleName the style name
     */
    public PageSliderIndicatorExt(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initPageSliderIndicator(context, attrSet);
    }

    private void initPageSliderIndicator(Context context, AttrSet attrSet) {
        mPageSliderIndicator = new PageSliderIndicator(context);
        DirectionalLayout.LayoutConfig indicatorLayoutConfig = new
                DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT);
        indicatorLayoutConfig.alignment = LayoutAlignment.CENTER;
        mPageSliderIndicator.setLayoutConfig(indicatorLayoutConfig);
        addComponent(mPageSliderIndicator);
    }

    /**
     * Sets page slider.
     *
     * @param pageSlider the page slider
     */
    public void setPageSlider(PageSlider pageSlider) {
        mPageSliderIndicator.setPageSlider(pageSlider);
    }

    /**
     * Sets item offset.
     *
     * @param offset the offset
     */
    public void setItemOffset(int offset) {
        mPageSliderIndicator.setItemOffset(offset);
    }

    /**
     * Sets item element.
     *
     * @param normal   the normal
     * @param selected the selected
     */
    public void setItemElement(Element normal, Element selected) {
        mPageSliderIndicator.setItemElement(normal, selected);
    }

}
