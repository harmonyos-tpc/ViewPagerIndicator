/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.viewpagerindicator;

import ohos.agp.components.AttrSet;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import com.viewpagerindicator.util.ResUtil;

import java.util.Optional;

/**
 * The type Underline page indicator.
 */
public class UnderlinePageIndicator extends PageSliderIndicatorExt {
    private static final String TAG = LinePageIndicator.class.getSimpleName();

    // custom attributes
    private static final String INDICATOR_NORMAL_COLOR = "indicator_normalColor";
    private static final String INDICATOR_SELECTED_COLOR = "indicator_selectedColor";
    private static final String INDICATOR_HEIGHT = "indicator_height";

    private Color indicatorNormalColor;
    private Color indicatorSelectedColor;
    private int indicatorHeight;
    private int indicatorWidth;
    private AttrSet mAttrSet;

    /**
     * Instantiates a new Underline page indicator.
     *
     * @param context the context
     */
    public UnderlinePageIndicator(Context context) {
        super(context);
        init(context, Optional.empty());
    }

    /**
     * Instantiates a new Underline page indicator.
     *
     * @param context the context
     * @param attrSet the attr set
     */
    public UnderlinePageIndicator(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, Optional.of(attrSet));
    }

    /**
     * Instantiates a new Underline page indicator.
     *
     * @param context   the context
     * @param attrSet   the attr set
     * @param styleName the style name
     */
    public UnderlinePageIndicator(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, Optional.of(attrSet));
    }

    private void init(Context context, Optional<AttrSet> optionalAttrSet)  {
        if (optionalAttrSet.isPresent()) {
            mAttrSet = optionalAttrSet.get();
            indicatorNormalColor = mAttrSet.getAttr(INDICATOR_NORMAL_COLOR).isPresent() ?
                    mAttrSet.getAttr(INDICATOR_NORMAL_COLOR).get().getColorValue() : Color.WHITE;
            indicatorSelectedColor = mAttrSet.getAttr(INDICATOR_SELECTED_COLOR).isPresent() ?
                    mAttrSet.getAttr(INDICATOR_SELECTED_COLOR).get().getColorValue() : Color.CYAN;
            indicatorHeight = mAttrSet.getAttr(INDICATOR_HEIGHT).isPresent() ?
                    mAttrSet.getAttr(INDICATOR_HEIGHT).get().getDimensionValue() : 8;
        }else {
            indicatorNormalColor = Color.WHITE;
            indicatorSelectedColor = Color.CYAN;
            indicatorHeight = 8;
        }
    }

    /**
     * Gets indicator normal color.
     *
     * @return the indicator normal color
     */
    public Color getIndicatorNormalColor() {
        return indicatorNormalColor;
    }

    /**
     * Sets indicator normal color.
     *
     * @param indicatorNormalColor the indicator normal color
     */
    public void setIndicatorNormalColor(Color indicatorNormalColor) {
        this.indicatorNormalColor = indicatorNormalColor;
    }

    /**
     * Gets indicator selected color.
     *
     * @return the indicator selected color
     */
    public Color getIndicatorSelectedColor() {
        return indicatorSelectedColor;
    }

    /**
     * Sets indicator selected color.
     *
     * @param indicatorSelectedColor the indicator selected color
     */
    public void setIndicatorSelectedColor(Color indicatorSelectedColor) {
        this.indicatorSelectedColor = indicatorSelectedColor;
    }

    /**
     * Gets indicator height.
     *
     * @return the indicator height
     */
    public int getIndicatorHeight() {
        return indicatorHeight;
    }

    /**
     * Sets indicator height.
     *
     * @param indicatorHeight the indicator height
     */
    public void setIndicatorHeight(int indicatorHeight) {
        this.indicatorHeight = indicatorHeight;
    }

    /**
     * Sets page slider.
     *
     * @param pageSlider the page slider
     */
    public void setPageSlider(PageSlider pageSlider) {
        super.setPageSlider(pageSlider);
        DisplayAttributes displayAttributes = DisplayManager.getInstance().getDefaultDisplay
                (getContext()).get().getAttributes();
        int screenWidth = displayAttributes.width;

        int tabCount = pageSlider.getProvider().getCount();
        indicatorWidth = screenWidth/tabCount;

        setItemElement(
                ResUtil.getCustomRectGradientDrawable(indicatorNormalColor,
                        new Rect(0, 0, indicatorWidth, 0)),
                ResUtil.getCustomRectGradientDrawable(indicatorSelectedColor,
                        new Rect(0, 0, indicatorWidth, indicatorHeight)));
    }

    /**
     * Sets item offset.
     *
     * @param offset the offset
     */
    public void setItemOffset(int offset) {
        super.setItemOffset(offset);
    }

    /**
     * Sets item element.
     *
     * @param normal   the normal
     * @param selected the selected
     */
    public void setItemElement(Element normal, Element selected) {
        super.setItemElement(normal, selected);
    }
}
