/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2012-2019. All rights reserved.
 */

package com.viewpagerindicator.sample;

import org.junit.Test;

/**
 * The type Main ability test.
 */
public class MainAbilityTest {
    /**
     * On start.
     */
    @Test
    public void onStart() {
    }
}
