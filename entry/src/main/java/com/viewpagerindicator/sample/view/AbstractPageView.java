/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.view;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;

import com.viewpagerindicator.sample.view.tab.TabInfo;
import com.viewpagerindicator.util.LogUtil;


/**
 * The type Abstract page view.
 */
public abstract class AbstractPageView implements PageInfo, TabInfo {
    private static final String TAG = AbstractPageView.class.getCanonicalName();
    /**
     * The Slice.
     */
    protected AbilitySlice slice;
    private String name;
    private int iconId = 0;
    private int iconIdSelected = 0;
    private Component rootView;
    private Color txtColor;

    /**
     * Instantiates a new Abstract page view.
     *
     * @param abilitySlice the ability slice
     * @param name         the name
     * @param txtColor     the txt color
     */
    public AbstractPageView(AbilitySlice abilitySlice, String name, Color txtColor) {
        if (abilitySlice != null) {
            this.slice = abilitySlice;
        } else {
            LogUtil.error(TAG, "slice is null, set default slice");
            this.slice = new AbilitySlice();
        }
        this.name = name;
        this.txtColor = txtColor;
        initView();
    }

    /**
     * Instantiates a new Abstract page view.
     *
     * @param abilitySlice   the ability slice
     * @param str            the str
     * @param iconId         the icon id
     * @param iconIdSelected the icon id selected
     * @param txtColor       the txt color
     */
    public AbstractPageView(AbilitySlice abilitySlice, String str, int iconId, int iconIdSelected, Color txtColor) {
        if (abilitySlice != null) {
            this.slice = abilitySlice;
        } else {
            LogUtil.error(TAG, "slice is null, set default slice");
            this.slice = new AbilitySlice();
        }
        this.name = str;
        this.iconId = iconId;
        this.iconIdSelected = iconIdSelected;
        this.txtColor = txtColor;
        initView();
    }

    /**
     * Init view.
     */
    public abstract void initView();

    /**
     * Gets slice.
     *
     * @return the slice
     */
    public AbilitySlice getSlice() {
        return this.slice;
    }

    public String getName() {
        return this.name;
    }

    public int getIconId() {
        return this.iconId;
    }

    /**
     * Sets icon id.
     *
     * @param id the id
     */
    public void setIconId(int id) {
        this.iconId = id;
    }

    public int getIconIdSelected() {
        return this.iconIdSelected;
    }

    /**
     * Sets icon id selected.
     *
     * @param id the id
     */
    public void setIconIdSelected(int id) {
        this.iconIdSelected = id;
    }

    /**
     * Gets txt color.
     *
     * @return the txt color
     */
    public Color getTxtColor() {
        return this.txtColor;
    }

    /**
     * Sets txt color.
     *
     * @param txtColor the txt color
     */
    public void setTxtColor(Color txtColor) {
        this.txtColor = txtColor;
    }

    public Component getRootView() {
        return this.rootView;
    }

    /**
     * Sets root view.
     *
     * @param component the component
     */
    public void setRootView(Component component) {
        this.rootView = component;
    }

}