/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.view;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

import com.viewpagerindicator.sample.ResourceTable;


/**
 * The type Sample page view.
 */
public class SamplePageView extends AbstractPageView {
    /**
     * Instantiates a new Sample page view.
     *
     * @param abilitySlice the ability slice
     * @param name         the name
     * @param txtColor     the txt color
     */
    public SamplePageView(AbilitySlice abilitySlice, String name, Color txtColor) {
        super(abilitySlice, name, txtColor);
    }

    /**
     * Instantiates a new Sample page view.
     *
     * @param abilitySlice   the ability slice
     * @param name           the name
     * @param iconId         the icon id
     * @param iconIdSelected the icon id selected
     * @param txtColor       the txt color
     */
    public SamplePageView(AbilitySlice abilitySlice, String name, int iconId, int iconIdSelected, Color txtColor) {
        super(abilitySlice, name, iconId, iconIdSelected, txtColor);
    }

    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    private Component loadView() {
        ComponentContainer layout = (ComponentContainer) LayoutScatter.getInstance(super.getSlice()).parse
                (ResourceTable.Layout_page_one, null, false);
        Text txtStatus = (Text) layout.findComponentById(ResourceTable.Id_txtStatus);
        txtStatus.setTextColor(getTxtColor());
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 16; i++) {
            builder.append(super.getName()).append(" ");
        }
        builder.deleteCharAt(builder.length() - 1);
        txtStatus.setText(builder.toString());
        return layout;
    }
}
