/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.components.TabList;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;

import com.viewpagerindicator.TabPageIndicator;
import com.viewpagerindicator.sample.ResourceTable;
import com.viewpagerindicator.sample.view.AbstractPageView;
import com.viewpagerindicator.sample.view.PageViewAdapter;
import com.viewpagerindicator.sample.view.SamplePageView;
import com.viewpagerindicator.sample.view.tab.TabInfo;
import com.viewpagerindicator.sample.view.tab.TabListListener;
import com.viewpagerindicator.util.ResUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * The type Sample tabs with icons slice.
 */
public class SampleTabsWithIconsSlice extends AbilitySlice {
    private DirectionalLayout mDlViewRoot;
    private PageSlider mPager;
    private TabPageIndicator mTabIndicator;

    private List<AbstractPageView> mPageViews;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(getContext()).parse
                (ResourceTable.Layout_slice_slider_tabs, null, false) instanceof ComponentContainer) {
            ComponentContainer layout = (ComponentContainer) LayoutScatter.getInstance(this).parse
                    (ResourceTable.Layout_slice_slider_tabs, null, false);
            mDlViewRoot = (DirectionalLayout) ResUtil.findComponentById(layout, ResourceTable.Id_dlRootLayout).get();
            mDlViewRoot.setBackground(ResUtil.buildDrawableByColor(new Color(0xFFCCCCCC).getValue()));
            mPager = (PageSlider) ResUtil.findComponentById(layout, ResourceTable.Id_slider).get();
            mTabIndicator = (TabPageIndicator) ResUtil.findComponentById(layout, ResourceTable.Id_tlTabLayout).get();
            initPageView();
            initTabs();
            setUIContent(layout);
        }
    }

    private void initPageView() {
        mPageViews = new ArrayList();
        mPageViews.add(new SamplePageView(this, "Recent",
                ResourceTable.Media_perm_group_calendar_normal,
                ResourceTable.Media_perm_group_calendar_selected, Color.BLACK));
        mPageViews.add(new SamplePageView(this, "Artists",
                ResourceTable.Media_perm_group_camera_normal,
                ResourceTable.Media_perm_group_camera_selected, Color.BLACK));
        mPageViews.add(new SamplePageView(this, "Albums",
                ResourceTable.Media_perm_group_device_alarms_normal,
                ResourceTable.Media_perm_group_device_alarms_selected, Color.BLACK));
        mPageViews.add(new SamplePageView(this, "Songs",
                ResourceTable.Media_perm_group_location_normal,
                ResourceTable.Media_perm_group_location_selected, Color.BLACK));
        mPager.setProvider(new PageViewAdapter(this, mPageViews));
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);
        mPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int itemPos, float itemPosOffset, int itemPosOffsetPixels) {
            }

            @Override
            public void onPageSlideStateChanged(int state) {
            }

            @Override
            public void onPageChosen(int position) {
                mTabIndicator.selectTab(mTabIndicator.getTabAt(position));
            }
        });
    }

    private void initTabs() {
        mTabIndicator.setTextSize(50);
        mTabIndicator.setTabLength(270);
        Element createBackground = ResUtil.buildDrawableByColor(Color.BLACK.getValue());
        mTabIndicator.setOrientation(Component.HORIZONTAL);
        mTabIndicator.setBackground(createBackground);
        mTabIndicator.setIndicatorColor(Color.RED.getValue());
        mTabIndicator.setIndicatorHeight(10);
        mTabIndicator.setCentralScrollMode(true);
        mTabIndicator.setTabTextAlignment(TextAlignment.VERTICAL_CENTER);
        mTabIndicator.setTabTextColors(Color.WHITE.getValue(), new Color(0x88FF0000).getValue());
        mTabIndicator.setViewPager(mPager);
        mTabIndicator.addTabSelectedListener(new TabListListener(this, mPageViews, mPager));
        mTabIndicator.setFixedMode(false);
        int tabPosition = 0;
        for (TabInfo next : mPageViews) {
            TabList tabList = mTabIndicator;
            Objects.requireNonNull(tabList);
            TabList.Tab tab = mTabIndicator.getTabAt(tabPosition);
            ResUtil.createTabIcon(this, tab, next.getIconId());
            tabPosition++;
        }
        mTabIndicator.selectTab(mTabIndicator.getTabAt(0));
    }
}
