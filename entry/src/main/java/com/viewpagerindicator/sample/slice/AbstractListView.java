/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Color;

import com.viewpagerindicator.sample.ResourceTable;
import com.viewpagerindicator.util.ResUtil;


/**
 * The type Abstract list view.
 */
public abstract class AbstractListView extends AbilitySlice {
    /**
     * The List component.
     */
    ListContainer list_component;
    private ComponentContainer mRootId;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_main_list, null, false) instanceof ComponentContainer) {
            ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_main_list, null, false);
            mRootId = (ComponentContainer) ResUtil.findComponentById(rootLayout, ResourceTable.Id_parentid).get();
            mRootId.setBackground(ResUtil.buildDrawableByColor((Color.DKGRAY).getValue()));
            super.setUIContent(rootLayout);
            list_component = (ListContainer) ResUtil.findComponentById(rootLayout, ResourceTable.Id_lc_main).get();
            populateListContainer();
        }
    }

    private void populateListContainer() {
        String[] list = loadListItems();
        list_component.setItemProvider(new AbstractListView.MainListProvider(list));
        list_component.setItemClickedListener(loadClickListener());
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        populateListContainer();
    }

    /**
     * Load list items string [ ].
     *
     * @return the string [ ]
     */
    protected abstract String[] loadListItems();

    /**
     * Load click listener list container . item clicked listener.
     *
     * @return the list container . item clicked listener
     */
    protected abstract ListContainer.ItemClickedListener loadClickListener();

    /**
     * The type Main list provider.
     */
    public class MainListProvider extends BaseItemProvider {
        /**
         * The List items.
         */
        String[] list_items;

        /**
         * Instantiates a new Main list provider.
         *
         * @param lst the lst
         */
        MainListProvider(String[] lst) {
            list_items = lst;
        }

        @Override
        public int getCount() {
            return list_items.length;
        }

        @Override
        public Object getItem(int position) {
            return list_items[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
            Component convertView = component;
            if (convertView == null) {
                convertView = LayoutScatter.getInstance(getContext()).parse
                        (ResourceTable.Layout_list_item, componentContainer, false);
            }
            ((Button) (ResUtil.findComponentById(convertView, ResourceTable.Id_list_component).get())).setText
                    ((String) getItem(position));
            convertView.setClickable(false);
            return convertView;
        }
    }
}
