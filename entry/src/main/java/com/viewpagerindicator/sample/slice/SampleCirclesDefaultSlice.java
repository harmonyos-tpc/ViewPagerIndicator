/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.sample.ResourceTable;
import com.viewpagerindicator.sample.view.AbstractPageView;
import com.viewpagerindicator.sample.view.PageViewAdapter;
import com.viewpagerindicator.sample.view.SamplePageView;
import com.viewpagerindicator.util.Const;
import com.viewpagerindicator.util.ResUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Sample circles default slice.
 */
public class SampleCirclesDefaultSlice extends AbilitySlice {
    private ComponentContainer mLayout;
    private DirectionalLayout mDlViewRoot;
    private PageSlider mPager;
    private CirclePageIndicator mIndicator;
    private PageViewAdapter pageViewAdapter;

    private List<AbstractPageView> mPageViews;
    private String[] pageText = {"This", "Is", "A", "Test"};
    private IntentParams intentParams;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        intentParams = intent.getParams();
        if (intentParams != null && intentParams.getParam(Const.KEY_VIA_LAYOUT) != null) {
            if (LayoutScatter.getInstance(getContext()).parse
                    (ResourceTable.Layout_slice_slider_circles_layout, null, false) instanceof ComponentContainer) {
                mLayout = (ComponentContainer) LayoutScatter.getInstance(this).parse
                        (ResourceTable.Layout_slice_slider_circles_layout, null, false);
            }
        } else {
            if (LayoutScatter.getInstance(getContext()).parse
                    (ResourceTable.Layout_slice_slider_circles, null, false) instanceof ComponentContainer) {
                mLayout = (ComponentContainer) LayoutScatter.getInstance(this).parse
                        (ResourceTable.Layout_slice_slider_circles, null, false);
            }
        }
        mDlViewRoot = (DirectionalLayout) ResUtil.findComponentById(mLayout, ResourceTable.Id_dlRootLayout).get();
        mDlViewRoot.setBackground(ResUtil.buildDrawableByColor((Color.DKGRAY).getValue()));
        mPager = (PageSlider) ResUtil.findComponentById(mLayout, ResourceTable.Id_slider).get();
        mIndicator = (CirclePageIndicator) ResUtil.findComponentById(mLayout, ResourceTable.Id_indicator).get();
        initPageView(intent);
        setUIContent(mLayout);

    }

    private void initPageView(Intent intent) {
        initPager(Color.WHITE);
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);
        mIndicator.setPageSlider(mPager);
        mIndicator.setItemOffset(17);

        if (intentParams != null) {
            if (intentParams.getParam(Const.KEY_INITIAL_PAGE) != null) {
                mPager.setCurrentPage(pageViewAdapter.getCount() - 1);
            }
            if (intentParams.getParam(Const.KEY_VIA_METHODS) != null) {
                initPager(Color.BLACK);
                mDlViewRoot.setBackground(ResUtil.buildDrawableByColor(new Color(0xFFCCCCCC).getValue()));
                mIndicator.setItemElement(
                        ResUtil.getCustomCircleGradientDrawable(2, new Color(0x880000FF), Color.BLACK,
                                new Rect(0, 0, 50, 50)),
                        ResUtil.getCustomCircleGradientDrawable(6, new Color(0xFF888888), Color.BLACK,
                                new Rect(0, 0, 50, 50)));
            }
            if (intentParams.getParam(Const.KEY_VIA_LAYOUT) != null) {
                initPager(Color.BLACK);
                mDlViewRoot.setBackground(ResUtil.buildDrawableByColor(new Color(0xFFCCCCCC).getValue()));
            }
            if (intentParams.getParam(Const.KEY_WITH_LISTENER) != null) {
                mPager.addPageChangedListener(new PageSlider.PageChangedListener() {
                    @Override
                    public void onPageSliding(int itemPos, float itemPosOffset, int itemPosOffsetPixels) {
                    }

                    @Override
                    public void onPageSlideStateChanged(int state) {
                    }

                    @Override
                    public void onPageChosen(int position) {
                        ResUtil.showToast(getContext(), "Changed to page " + (position + 1), 2000);
                    }
                });
            }
        }
    }

    private void initPager(Color color) {
        mPageViews = new ArrayList();
        mPageViews.add(new SamplePageView(this, pageText[0], color));
        mPageViews.add(new SamplePageView(this, pageText[1], color));
        mPageViews.add(new SamplePageView(this, pageText[2], color));
        mPageViews.add(new SamplePageView(this, pageText[3], color));
        pageViewAdapter = new PageViewAdapter(this, mPageViews);
        mPager.setProvider(pageViewAdapter);
    }

}