/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import static com.viewpagerindicator.util.Const.KEY_FADE;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;

import com.viewpagerindicator.UnderlinePageIndicator;
import com.viewpagerindicator.sample.ResourceTable;
import com.viewpagerindicator.sample.view.AbstractPageView;
import com.viewpagerindicator.sample.view.PageViewAdapter;
import com.viewpagerindicator.sample.view.SamplePageView;
import com.viewpagerindicator.util.Const;
import com.viewpagerindicator.util.ResUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Sample under lines default slice.
 */
public class SampleUnderLinesDefaultSlice extends AbilitySlice {
    private ComponentContainer mLayout;
    private DirectionalLayout mDlViewRoot;
    private DirectionalLayout mDlViewParent;
    private PageSlider mPager;
    private UnderlinePageIndicator mColorIndicator;
    private PageViewAdapter pageViewAdapter;

    private List<AbstractPageView> mPageViews;
    private String[] pageText = {"This", "Is", "A", "Test"};
    private boolean isFade;
    private IntentParams intentParams;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        intentParams = intent.getParams();
        if (intentParams != null && intentParams.getParam(Const.KEY_VIA_LAYOUT) != null) {
            if (LayoutScatter.getInstance(getContext()).parse
                    (ResourceTable.Layout_slice_slider_underlines_layout, null, false) instanceof ComponentContainer) {
                mLayout = (ComponentContainer) LayoutScatter.getInstance(this).parse
                        (ResourceTable.Layout_slice_slider_underlines_layout, null, false);
            }
        } else {
            if (LayoutScatter.getInstance(getContext()).parse
                    (ResourceTable.Layout_slice_slider_underlines, null, false) instanceof ComponentContainer) {
                mLayout = (ComponentContainer) LayoutScatter.getInstance(this).parse
                        (ResourceTable.Layout_slice_slider_underlines, null, false);
            }
        }
        if (intentParams != null) {
            isFade = (boolean) intentParams.getParam(KEY_FADE);
        }
        mDlViewRoot = (DirectionalLayout) ResUtil.findComponentById(mLayout, ResourceTable.Id_dlRootLayout).get();
        mDlViewRoot.setBackground(ResUtil.buildDrawableByColor((Color.BLACK).getValue()));
        mDlViewParent = (DirectionalLayout) ResUtil.findComponentById(mLayout, ResourceTable.Id_dlViewParent).get();
        mDlViewParent.setBackground(ResUtil.buildDrawableByColor((Color.DKGRAY).getValue()));
        mPager = (PageSlider) ResUtil.findComponentById(mLayout, ResourceTable.Id_slider).get();
        mColorIndicator = (UnderlinePageIndicator)
                ResUtil.findComponentById(mLayout, ResourceTable.Id_indicator).get();
        initPageView(intent);
        setUIContent(mLayout);
    }

    private void initPageView(Intent intent) {
        initPager(Color.WHITE);
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);
        mColorIndicator.setPageSlider(mPager);
        mColorIndicator.setItemOffset(0);
        if (intentParams != null) {
            if (intentParams.getParam(Const.KEY_VIA_METHODS) != null) {
                initPager(Color.BLACK);
                mDlViewParent.setBackground(ResUtil.buildDrawableByColor(new Color(0xFFCCCCCC).getValue()));
                mColorIndicator.setItemElement(
                        ResUtil.getCustomRectGradientDrawable(Color.DKGRAY, new Rect(0, 0, 270, 0)),
                        ResUtil.getCustomRectGradientDrawable(Color.RED, new Rect(0, 0, 270, 10)));
            }
        }
        if (intentParams.getParam(Const.KEY_VIA_LAYOUT) != null) {
            initPager(Color.BLACK);
            mDlViewRoot.setBackground(ResUtil.buildDrawableByColor(new Color(0xFFCCCCCC).getValue()));
        }
        addPageChangedListener();
    }

    private void addPageChangedListener() {
        AnimatorValue inAnim = new AnimatorValue();
        inAnim.setDuration(50);
        AnimatorValue outAnim = new AnimatorValue();
        outAnim.setDuration(800);
        if (isFade) {
            outAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float value) {
                    mColorIndicator.setAlpha(Float.valueOf(1) - value);
                }
            });
            outAnim.start();
        }
        mPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            /**
             * onPageSlideStateChanged
             *
             * @param state The Current state
             */
            public void onPageSlideStateChanged(int state) {
                if (isFade) {
                    if (state == 1) {
                        inAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                            @Override
                            public void onUpdate(AnimatorValue animatorValue, float value) {
                                mColorIndicator.setAlpha(value);
                            }
                        });
                        inAnim.start();
                    }
                }
            }

            /**
             * onPageSliding
             *
             * @param itemPos Items Position
             * @param itemPosOffset Item Position Offset
             * @param itemPosOffsetPixels Item Position Offset in Pixels
             */
            public void onPageSliding(int itemPos, float itemPosOffset, int itemPosOffsetPixels) {
                if (isFade) {
                    if (itemPosOffset > 0.9) {
                        outAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                            @Override
                            public void onUpdate(AnimatorValue animatorValue, float value) {
                                mColorIndicator.setAlpha(Float.valueOf(1) - value);
                            }
                        });
                        outAnim.start();
                    }
                }
            }

            /**
             * onPageChosen
             *
             * @param position Current Position
             */
            public void onPageChosen(int position) {
            }
        });
    }

    private void initPager(Color color) {
        mPageViews = new ArrayList();
        mPageViews.add(new SamplePageView(this, pageText[0], color));
        mPageViews.add(new SamplePageView(this, pageText[1], color));
        mPageViews.add(new SamplePageView(this, pageText[2], color));
        mPageViews.add(new SamplePageView(this, pageText[3], color));
        pageViewAdapter = new PageViewAdapter(this, mPageViews);
        mPager.setProvider(pageViewAdapter);
    }
}
