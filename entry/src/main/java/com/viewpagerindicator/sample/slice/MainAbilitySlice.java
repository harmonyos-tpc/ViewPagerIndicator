/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.PositionLayout;
import ohos.agp.utils.Color;

import com.viewpagerindicator.sample.ResourceTable;
import com.viewpagerindicator.util.LogUtil;
import com.viewpagerindicator.util.ResUtil;


/**
 * The type Main ability slice.
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final String KEY_FADE = "fade";
    /**
     * The List.
     */
    String[] list = {"Circles", "Icons", "Lines", "Tabs", "Titles ", "UnderLines"};
    private PositionLayout mRootId;
    private ListContainer list_component;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(getContext()).parse
                (ResourceTable.Layout_main_list, null, false) instanceof ComponentContainer) {
            ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance
                    (getContext()).parse(ResourceTable.Layout_main_list, null, false);
            mRootId = (PositionLayout) ResUtil.findComponentById(rootLayout, ResourceTable.Id_parentid).get();
            mRootId.setBackground(ResUtil.buildDrawableByColor((Color.DKGRAY).getValue()));
            list_component = (ListContainer) ResUtil.findComponentById(rootLayout, ResourceTable.Id_lc_main).get();
            populateListContainer();
            super.setUIContent(rootLayout);
        }
    }

    @Override
    protected void onForeground(Intent intent) {
        LogUtil.info("MainScreen", "onForeground");
        super.onForeground(intent);
        populateListContainer();
    }

    private void populateListContainer() {
        list_component.setItemProvider(new MainListProvider(list));
        list_component.setItemClickedListener((listContainer, component, position, id) -> {
            Intent intent1 = new Intent();
            intent1.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
            switch (position) {
                case 0:
                    present(new CirclesListSlice(), intent1);
                    break;
                case 1:
                    present(new IconsListSlice(), intent1);
                    break;
                case 2:
                    present(new LinesListSlice(), intent1);
                    break;
                case 3:
                    present(new TabsListSlice(), intent1);
                    break;
                case 4:
                    present(new TitlesListSlice(), intent1);
                    break;
                case 5:
                    present(new UnderLinesListSlice(), intent1);
                    break;
            }
        });
    }

    /**
     * The type Main list provider.
     */
    public class MainListProvider extends BaseItemProvider {
        /**
         * The List items.
         */
        String[] list_items;

        /**
         * Instantiates a new Main list provider.
         *
         * @param lst the lst
         */
        MainListProvider(String[] lst) {
            list_items = lst;
        }

        @Override
        public int getCount() {
            return list_items.length;
        }

        @Override
        public Object getItem(int position) {
            return list_items[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
            Component convertView = component;
            if (convertView == null) {
                convertView = LayoutScatter.getInstance(getContext())
                        .parse(ResourceTable.Layout_list_item, componentContainer, false);
            }
            ((Button) (ResUtil.findComponentById(convertView, ResourceTable.Id_list_component).get()))
                    .setText((String) getItem(position));
            convertView.setClickable(false);
            return convertView;
        }
    }
}
