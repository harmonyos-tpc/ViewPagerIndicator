/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Color;

import com.viewpagerindicator.TitlePageIndicator;
import com.viewpagerindicator.sample.ResourceTable;
import com.viewpagerindicator.sample.view.AbstractPageView;
import com.viewpagerindicator.sample.view.PageViewAdapter;
import com.viewpagerindicator.sample.view.SamplePageView;
import com.viewpagerindicator.util.Const;
import com.viewpagerindicator.util.ResUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Sample titles default slice.
 */
public class SampleTitlesDefaultSlice extends AbilitySlice implements TitlePageIndicator.OnCenterItemClickListener {
    private ComponentContainer mLayout;
    private DirectionalLayout mDlViewRoot;
    private PageSlider mPager;
    private TitlePageIndicator mIndicator;
    private PageViewAdapter pageViewAdapter;

    private List<AbstractPageView> mPageViews;
    private String[] pageText = {"This", "Is", "A", "Test"};
    private IntentParams intentParams;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        intentParams = intent.getParams();
        if (intentParams != null && intentParams.getParam(Const.KEY_BOTTOM) != null) {
            if (LayoutScatter.getInstance(getContext()).parse
                    (ResourceTable.Layout_slice_slider_titles_bottom, null, false)
                    instanceof ComponentContainer) {
                mLayout = (ComponentContainer) LayoutScatter.getInstance(this).parse
                        (ResourceTable.Layout_slice_slider_titles_bottom, null, false);
                mIndicator = (TitlePageIndicator) ResUtil.findComponentById(mLayout, ResourceTable.Id_indicator).get();
                mIndicator.setLinePosition(TitlePageIndicator.LinePosition.Top);
            }
        } else {
            if (LayoutScatter.getInstance(getContext()).parse
                    (ResourceTable.Layout_slice_slider_titles, null, false)
                    instanceof ComponentContainer) {
                mLayout = (ComponentContainer) LayoutScatter.getInstance(this).parse
                        (ResourceTable.Layout_slice_slider_titles, null, false);
                mIndicator = (TitlePageIndicator) ResUtil.findComponentById(mLayout, ResourceTable.Id_indicator).get();
            }
        }
        mDlViewRoot = (DirectionalLayout) ResUtil.findComponentById(mLayout, ResourceTable.Id_dlRootLayout).get();
        mDlViewRoot.setBackground(ResUtil.buildDrawableByColor((Color.DKGRAY).getValue()));
        mPager = (PageSlider) ResUtil.findComponentById(mLayout, ResourceTable.Id_slider).get();

        initPageView();
        setUIContent(mLayout);
    }

    private void initPageView() {
        initPager(Color.WHITE);
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);
        mIndicator.setPageSlider(mPager);
        mIndicator.setFooterColor(Color.CYAN.getValue());
        mIndicator.setTextColor(Color.CYAN.getValue());
        if (intentParams != null) {
            if (intentParams.getParam(Const.KEY_INITIAL_PAGE) != null) {
                mPager.setCurrentPage(pageViewAdapter.getCount() - 1);
            }
            if (intentParams.getParam(Const.KEY_CENTER_CLICK_LISTENER) != null) {
                mIndicator.setOnCenterItemClickListener(this::onCenterItemClick);
            }
            if (intentParams.getParam(Const.KEY_TRIANGLE) != null) {
                mIndicator.setFooterIndicatorStyle(TitlePageIndicator.IndicatorStyle.Triangle);
            }
            if (intentParams.getParam(Const.KEY_VIA_METHODS) != null) {
                initPager(Color.BLACK);
                mDlViewRoot.setBackground(ResUtil.buildDrawableByColor(new Color(0xFFCCCCCC).getValue()));
                final float density = getResourceManager().getDeviceCapability().screenDensity;
                mIndicator.setBackground(ResUtil.buildDrawableByColor(new Color(0x18FF0000).getValue()));
                mIndicator.setFooterColor(0xFFAA2222);
                mIndicator.setFooterIndicatorStyle(TitlePageIndicator.IndicatorStyle.Underline);
                mIndicator.setTextColor(0xAA000000);
                mIndicator.setSelectedColor(0xFF000000);
                mIndicator.setSelectedBold(true);
            }
            if (intentParams.getParam(Const.KEY_VIA_LAYOUT) != null) {
                initPager(Color.BLACK);
                mDlViewRoot.setBackground(ResUtil.buildDrawableByColor(new Color(0xFFCCCCCC).getValue()));
                mIndicator.setStyleFromLayoutAttributes();
            }
            if (intentParams.getParam(Const.KEY_WITH_LISTENER) != null) {
                mPager.addPageChangedListener(new PageSlider.PageChangedListener() {
                    @Override
                    public void onPageSliding(int itemPos, float itemPosOffset, int itemPosOffsetPixels) {
                    }

                    @Override
                    public void onPageSlideStateChanged(int state) {
                    }

                    @Override
                    public void onPageChosen(int position) {
                        ResUtil.showToast(getContext(), "Changed to page " + (position + 1), 2000);
                    }
                });
            }
        }
    }

    private void initPager(Color color) {
        mPageViews = new ArrayList();
        mPageViews.add(new SamplePageView(this, pageText[0], color));
        mPageViews.add(new SamplePageView(this, pageText[1], color));
        mPageViews.add(new SamplePageView(this, pageText[2], color));
        mPageViews.add(new SamplePageView(this, pageText[3], color));
        pageViewAdapter = new PageViewAdapter(this, mPageViews);
        mPager.setProvider(pageViewAdapter);
    }

    @Override
    public void onCenterItemClick(int position) {
        ResUtil.showToast(getContext(), "You clicked the center title! ", 2000);
    }

}
