/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;

import com.viewpagerindicator.util.Const;


/**
 * The type Titles list slice.
 */
public class TitlesListSlice extends AbstractListView {
    @Override
    protected String[] loadListItems() {
        return new String[]{"Center Click Listener", "Default", "Default (Bottom)",
                "InitialPage", "Styled (via layouts)", "Styled (via methods)",
                "Styled (via theme) - Not Implemented", "Triangle Style", "With Listener"};
    }

    @Override
    protected ListContainer.ItemClickedListener loadClickListener() {
        return (listContainer, component, position, id) -> {
            Intent intent = new Intent();
            intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
            switch (position) {
                case 0:
                    intent.setParam(Const.KEY_CENTER_CLICK_LISTENER, true);
                    present(new SampleTitlesDefaultSlice(), intent);
                    break;
                case 1:
                    present(new SampleTitlesDefaultSlice(), intent);
                    break;
                case 2:
                    intent.setParam(Const.KEY_BOTTOM, true);
                    present(new SampleTitlesDefaultSlice(), intent);
                    break;
                case 3:
                    intent.setParam(Const.KEY_INITIAL_PAGE, true);
                    present(new SampleTitlesDefaultSlice(), intent);
                    break;
                case 4:
                    intent.setParam(Const.KEY_VIA_LAYOUT, true);
                    present(new SampleTitlesDefaultSlice(), intent);
                    break;
                case 6:
                    break;
                case 5:
                    intent.setParam(Const.KEY_VIA_METHODS, true);
                    present(new SampleTitlesDefaultSlice(), intent);
                    break;
                case 7:
                    intent.setParam(Const.KEY_TRIANGLE, true);
                    present(new SampleTitlesDefaultSlice(), intent);
                    break;
                case 8:
                    intent.setParam(Const.KEY_WITH_LISTENER, true);
                    present(new SampleTitlesDefaultSlice(), intent);
                    break;
            }
        };
    }
}