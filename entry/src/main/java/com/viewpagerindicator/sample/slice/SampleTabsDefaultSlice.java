/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;

import com.viewpagerindicator.TabPageIndicator;
import com.viewpagerindicator.sample.ResourceTable;
import com.viewpagerindicator.sample.view.AbstractPageView;
import com.viewpagerindicator.sample.view.PageViewAdapter;
import com.viewpagerindicator.sample.view.SamplePageView;
import com.viewpagerindicator.sample.view.tab.TabListListener;
import com.viewpagerindicator.util.Const;
import com.viewpagerindicator.util.ResUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Sample tabs default slice.
 */
public class SampleTabsDefaultSlice extends AbilitySlice {
    private DirectionalLayout mDlViewRoot;
    private PageSlider mPager;
    private TabPageIndicator mTabIndicator;
    private PageViewAdapter pageViewAdapter;

    private List<AbstractPageView> mPageViews;
    private String[] pageText = {"This", "Is", "A", "Test"};

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(getContext()).parse
                (ResourceTable.Layout_slice_slider_tabs, null, false) instanceof ComponentContainer) {
            ComponentContainer layout = (ComponentContainer) LayoutScatter.getInstance(this).parse
                    (ResourceTable.Layout_slice_slider_tabs, null, false);
            mDlViewRoot = (DirectionalLayout) layout.findComponentById(ResourceTable.Id_dlRootLayout);
            mDlViewRoot.setBackground(ResUtil.buildDrawableByColor((Color.DKGRAY).getValue()));
            mPager = (PageSlider) layout.findComponentById(ResourceTable.Id_slider);
            mTabIndicator = (TabPageIndicator) layout.findComponentById(ResourceTable.Id_tlTabLayout);
            initPageView(intent);
            initTabs();
            setUIContent(layout);
        }
    }

    private void initPageView(Intent intent) {
        initPager(Color.WHITE);
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);

        IntentParams intentParams = intent.getParams();
        if (intentParams != null) {
            if (intentParams.getParam(Const.KEY_VIA_LAYOUT) != null) {
                initPager(Color.BLACK);
                mDlViewRoot.setBackground(ResUtil.buildDrawableByColor(new Color(0xFFCCCCCC).getValue()));
            }
        }
        mPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int itemPos, float itemPosOffset, int itemPosOffsetPixels) {
            }

            @Override
            public void onPageSlideStateChanged(int state) {
            }

            @Override
            public void onPageChosen(int position) {
                mTabIndicator.selectTab(mTabIndicator.getTabAt(position));
            }
        });
    }

    private void initTabs() {
        mTabIndicator.setTabTextSize(50);
        mTabIndicator.setTabLength(250);
        Element createBackground = ResUtil.buildDrawableByColor(Color.BLACK.getValue());
        mTabIndicator.setOrientation(Component.HORIZONTAL);
        mTabIndicator.setBackground(createBackground);
        mTabIndicator.setCentralScrollMode(true);
        mTabIndicator.setTabTextAlignment(TextAlignment.CENTER);
        mTabIndicator.setTabTextColors(Color.WHITE.getValue(), Color.CYAN.getValue());
        mTabIndicator.setViewPager(mPager);
        mTabIndicator.addTabSelectedListener(new TabListListener(this, mPageViews, mPager));
        mTabIndicator.setFixedMode(false);
        mTabIndicator.selectTab(mTabIndicator.getTabAt(0));
    }

    private void initPager(Color color) {
        mPageViews = new ArrayList();
        mPageViews.add(new SamplePageView(this, pageText[0], color));
        mPageViews.add(new SamplePageView(this, pageText[1], color));
        mPageViews.add(new SamplePageView(this, pageText[2], color));
        mPageViews.add(new SamplePageView(this, pageText[3], color));
        pageViewAdapter = new PageViewAdapter(this, mPageViews);
        mPager.setProvider(pageViewAdapter);
    }

}

