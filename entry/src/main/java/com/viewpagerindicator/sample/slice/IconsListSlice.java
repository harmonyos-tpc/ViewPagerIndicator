/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;


/**
 * The type Icons list slice.
 */
public class IconsListSlice extends AbstractListView {
    @Override
    protected String[] loadListItems() {
        return new String[]{"Default"
        };
    }

    @Override
    protected ListContainer.ItemClickedListener loadClickListener() {
        return (listContainer, component, position, id) -> {
            Intent intent = new Intent();
            intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
            switch (position) {
                case 0:
                    IconsListSlice.this.present(new SampleIconsDefaultSlice(), intent);
                    break;
            }
        };
    }
}