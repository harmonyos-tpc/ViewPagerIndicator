/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.viewpagerindicator.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.components.TabList;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

import com.viewpagerindicator.IconPageIndicator;
import com.viewpagerindicator.sample.ResourceTable;
import com.viewpagerindicator.sample.view.AbstractPageView;
import com.viewpagerindicator.sample.view.PageViewAdapter;
import com.viewpagerindicator.sample.view.SamplePageView;
import com.viewpagerindicator.sample.view.tab.TabInfo;
import com.viewpagerindicator.util.ResUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


/**
 * The type Sample icons default slice.
 */
public class SampleIconsDefaultSlice extends AbilitySlice {
    private DirectionalLayout mDlViewRoot;
    private PageSlider mPager;
    private IconPageIndicator mTabIndicator;

    private List<AbstractPageView> mPageViews;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(getContext()).parse
                (ResourceTable.Layout_slice_slider_icons, null, false) instanceof ComponentContainer) {
            ComponentContainer layout = (ComponentContainer) LayoutScatter.getInstance(this).parse
                    (ResourceTable.Layout_slice_slider_icons, null, false);
            mDlViewRoot = (DirectionalLayout) ResUtil.findComponentById(layout, ResourceTable.Id_dlRootLayout).get();
            mDlViewRoot.setBackground(ResUtil.buildDrawableByColor(Color.DKGRAY.getValue()));
            mPager = (PageSlider) ResUtil.findComponentById(layout, ResourceTable.Id_slider).get();
            mTabIndicator = (IconPageIndicator) layout.findComponentById(ResourceTable.Id_tlTabLayout);
            initPageView();
            initTabs();
            setUIContent(layout);
        }
    }

    private void initPageView() {
        mPageViews = new ArrayList();
        mPageViews.add(new SamplePageView(this, "Recent",
                ResourceTable.Media_perm_group_calendar_normal,
                ResourceTable.Media_perm_group_calendar_selected, Color.WHITE));
        mPageViews.add(new SamplePageView(this, "Artists",
                ResourceTable.Media_perm_group_camera_normal,
                ResourceTable.Media_perm_group_camera_selected, Color.WHITE));
        mPageViews.add(new SamplePageView(this, "Albums",
                ResourceTable.Media_perm_group_device_alarms_normal,
                ResourceTable.Media_perm_group_device_alarms_selected, Color.WHITE));
        mPageViews.add(new SamplePageView(this, "Songs",
                ResourceTable.Media_perm_group_location_normal,
                ResourceTable.Media_perm_group_location_selected, Color.WHITE));
        mPager.setProvider(new PageViewAdapter(this, mPageViews));
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);
        mPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int itemPos, float itemPosOffset, int itemPosOffsetPixels) {
            }

            @Override
            public void onPageSlideStateChanged(int state) {
            }

            @Override
            public void onPageChosen(int position) {
                mTabIndicator.selectTab(mTabIndicator.getTabAt(position));
            }
        });
    }

    private void initTabs() {
        Element createBackground = ResUtil.buildDrawableByColor(Color.DKGRAY.getValue());
        mTabIndicator.setOrientation(Component.HORIZONTAL);
        mTabIndicator.setBackground(createBackground);
        mTabIndicator.setCentralScrollMode(true);
        mTabIndicator.setViewPager(mPager);
        int tabPosition = 0;
        for (TabInfo next : mPageViews) {
            TabList tabList = mTabIndicator;
            Objects.requireNonNull(tabList);
            TabList.Tab tab = mTabIndicator.getTabAt(tabPosition);
            ResUtil.createIcons(this, tab, next.getIconId());
            tab.setBackground(ResUtil.buildDrawableByColor(Color.DKGRAY.getValue()));
            tabPosition++;
        }
        mTabIndicator.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                PageSlider pageSlider = mPager;
                if (pageSlider != null) {
                    pageSlider.setCurrentPage(tab.getPosition());
                }
                Optional<TabInfo> associateTabView = getAssociateTabView(tab);
                if (associateTabView.isPresent()) {
                    if (associateTabView.get().getIconIdSelected() != 0) {
                        ResUtil.createIcons(SampleIconsDefaultSlice.this, tab,
                                associateTabView.get().getIconIdSelected());
                    }
                }
            }

            @Override
            public void onUnselected(TabList.Tab tab) {
                Optional<TabInfo> associateTabView = getAssociateTabView(tab);
                if (associateTabView.isPresent()) {
                    if (associateTabView.get().getIconId() != 0) {
                        ResUtil.createTabIcon(SampleIconsDefaultSlice.this, tab, associateTabView.get().getIconId());
                    }
                }
            }

            @Override
            public void onReselected(TabList.Tab tab) {
            }
        });
        mTabIndicator.setFixedMode(true);
        mTabIndicator.selectTab(mTabIndicator.getTabAt(0));
    }

    private Optional<TabInfo> getAssociateTabView(TabList.Tab tab) {
        int position = tab.getPosition();
        if (position >= 0 && position <= mPageViews.size()) {
            return Optional.ofNullable((TabInfo) mPageViews.get(position));
        }
        return Optional.empty();
    }

}